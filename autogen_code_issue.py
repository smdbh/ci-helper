import codecs
import os
import re
import sys
import requests
from functools import partial
import chardet

directory_path = "."
KEYWORD_GRP = "BUG|TODO|FIXME"


def search_keyword_in_file(file_path, keyword):
    matching_lines = []

    codeName = 'utf-8'
    with open(file_path, 'rb') as f:
        codeName = chardet.detect(f.read())['encoding']
    # print(f"file: {file_path=},{codeName=} ")

    with codecs.open(file_path, encoding=codeName) as file:
        lines = file.readlines()

        for line_num, line in enumerate(lines, 1):
            if re.search(keyword, line):
                matching_lines.append([file_path, line_num, line.replace('//', '').strip()])
    return matching_lines


def find_todos(directory, extensions=''):
    findFiles = []
    findTodos = []
    for root, dirs, files in os.walk(directory):
        rel_dir = os.path.relpath(root, directory)
        for file in files:
            if file.endswith(extensions):
                # print(f"find: {rel_dir=}, {file=} ")
                findFiles.append(os.path.join(rel_dir, file))
                file_path = os.path.join(root, file)
                ret = search_keyword_in_file(file_path, f'//[ ]{{1,2}}({KEYWORD_GRP}):')
                if ret:
                    findTodos.extend(ret)
                    # ret[0] = rel_dir = os.path.relpath(ret[0], directory)
                    print(f"find {ret=} ")
    return findTodos


class gitlabHelper:

    def __init__(self, url='', projectid=1, access_token=''):
        self.api_url = url
        self.access_token = access_token
        self.projectId = projectid
        requests.packages.urllib3.disable_warnings()

        headers = {"PRIVATE-TOKEN": self.access_token}
        self._post = partial(requests.post, headers=headers, verify=False)
        self._get = partial(requests.get, headers=headers, verify=False)
        self._delete = partial(requests.delete, headers=headers, verify=False)

        pass

    def set_projectid(self, pid=1):
        self.projectId = pid

    def get_project_issue(self):
        issueList = []

        issues_url = f"{self.api_url}/projects/{self.projectId}/issues"
        headers = {"PRIVATE-TOKEN": self.access_token}

        try:
            # requests.packages.urllib3.disable_warnings()
            response = self._get(issues_url)
            print(f"response: {response.status_code=} ")
            if response.status_code == 200:
                # 提取问题列表
                issues = response.json()

                # 打印问题标题和状态
                for issue in issues:
                    print(f"Issue: {issue['title']} - State: {issue['state']}")
                    issueList.append(issue)

        except Exception as e:
            print(f"error {e.args=} ")
            pass

        return issueList

    def add_project_issue(self, title='?', content='', label=None):
        url = f"{self.api_url}/projects/{self.projectId}/issues"

        data = {
            'title': title,
            'description': content,
            'add_labels': label
        }
        response = '?'
        try:
            response = self._post(url, json=data)
            print(f"add issue: {response.status_code} ")
        except Exception as e:
            print(f'exception: {e.args}')

    def del_project_issue(self, iid=1):
        url = f"{self.api_url}/projects/{self.projectId}/issues/{iid}"
        response = '?'
        try:
            response = self._delete(url)
        except Exception as e:
            print(f'exception: {e.args}')
        finally:
            print(f"delete issue {iid}: {response.status_code} ")

        pass


def delete_autogen_issues(gh, issueList):
    print(f"start delete exist issue...")
    for issue in issueList:
        title = issue.get('title', '')
        if re.match(f'^::({KEYWORD_GRP}):', title):
            gh.del_project_issue(issue.get('iid'))
    pass


def main(url='', pid=1, actoken=''):
    print(f"hello ci-job... ")

    findTodos = find_todos(directory_path, ('.c', '.h'))
    # create issue
    # [code nav](src / main.c  # L10-12)
    gh = gitlabHelper(url, pid, actoken)
    data = gh.get_project_issue()
    # print(f"get issues: {data=} ")
    delete_autogen_issues(gh, data)

    # add code issues
    for item in findTodos:
        relpath = os.path.relpath(item[0], directory_path)
        title = '::' + item[2]
        if len(title) < 8:  # means content too short
            title += relpath

        message = f"[{title}]({relpath}#L{item[1]})"
        gh.add_project_issue(title, str(message), label=('autogen'))

    pass

if __name__ == '__main__':
    token = '?'
    pid = 1
    api_url = "http://192.168.1.21/api/v4"

    for v in sys.argv:
        print(f">>> {v=} ")


    if len(sys.argv) < 4:
        print(f"args missing ")
    else:
        api_url = sys.argv[1]
        pid = int(sys.argv[2], 10)
        token = sys.argv[3]


    print(f"args: {api_url} {pid=} {token=} ")
    main(api_url, pid, token)
