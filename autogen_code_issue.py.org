import codecs
import os
import re
import sys

import requests
from functools import partial

directory_path = "."


def search_keyword_in_file(file_path, keyword):
    matching_lines = []
    # with open(file_path, 'r') as file:
    with codecs.open(file_path, encoding='utf-8') as file:
        lines = file.readlines()
        for line_num, line in enumerate(lines, 1):
            if re.search(keyword, line):
                matching_lines.append([file_path, line_num, line.replace('//', '').strip()])
    return matching_lines


def find_todos(directory, extensions=[]):
    findFiles = []
    findTodos = []
    for root, dirs, files in os.walk(directory):
        rel_dir = os.path.relpath(root, directory)
        for file in files:
            if file.endswith(extensions):
                # print(f"find: {rel_dir=}, {file=} ")
                findFiles.append(os.path.join(rel_dir, file))
                file_path = os.path.join(root, file)
                ret = search_keyword_in_file(file_path, '//[ ]{1,2}(TODO|FIXME):')
                if ret:
                    findTodos.extend(ret)
                    # ret[0] = rel_dir = os.path.relpath(ret[0], directory)
                    print(f"find {ret=} ")
    return findTodos


class gitlabHelper:
    api_url = "https://gitlab.com/api/v4"

    def __init__(self, access_token=''):
        self.access_token = access_token
        self.projectId = 51663032
        requests.packages.urllib3.disable_warnings()

        headers = {"PRIVATE-TOKEN": self.access_token}
        self._post = partial(requests.post, headers=headers, verify=False)
        self._get = partial(requests.get, headers=headers, verify=False)
        self._delete = partial(requests.delete, headers=headers, verify=False)

        pass

    def set_projectid(self, pid=1):
        self.projectId = pid

    def get_project_issue(self):
        issueList = []

        issues_url = f"{self.api_url}/projects/{self.projectId}/issues"
        headers = {"PRIVATE-TOKEN": self.access_token}

        try:
            # requests.packages.urllib3.disable_warnings()
            response = self._get(issues_url)
            print(f"response: {response.status_code=} ")
            if response.status_code == 200:
                # 提取问题列表
                issues = response.json()

                # 打印问题标题和状态
                for issue in issues:
                    print(f"Issue: {issue['title']} - State: {issue['state']}")
                    issueList.append(issue)

        except Exception as e:
            print(f"error {e.args=} ")
            pass

        return issueList

    def add_project_issue(self, title='?', content=''):
        url = f"{self.api_url}/projects/{self.projectId}/issues"

        data = {
            'title': title,
            'description': content
        }
        response = '?'
        try:
            response = self._post(url, json=data)
        except Exception as e:
            print(f'exception: {e.args}')
        finally:
            print(f"add issue: {response.status_code} ")

    def del_project_issue(self, iid=1):
        url = f"{self.api_url}/projects/{self.projectId}/issues/{iid}"
        response = '?'
        try:
            response = self._delete(url)
        except Exception as e:
            print(f'exception: {e.args}')
        finally:
            print(f"delete issue {iid}: {response.status_code} ")

        pass


def delete_autogen_issues(gh, issueList):
    print(f"start dele")
    for issue in issueList:
        gh.del_project_issue(issue.get('iid'))
    pass


def main(actoken=''):
    print(f"hello ci-job... ")

    findTodos = find_todos(directory_path, ('.c', '.h'))
    #
    # # # 打印结果
    # for lines in findTodos:
    #     print('> ', lines)
    # pass
    gh = gitlabHelper(access_token=actoken)
    # gh.get_project_issue()

    # delete_autogen_issues(gh, gh.get_project_issue())

    for i in range(23, 24):
        gh.add_project_issue(f'problem: {i}', f'des {i}')


if __name__ == '__main__':
    token = '?'
    if len(sys.argv) < 2:
        print(f"args missing ")
        token = sys.argv[1]
    main(token)
